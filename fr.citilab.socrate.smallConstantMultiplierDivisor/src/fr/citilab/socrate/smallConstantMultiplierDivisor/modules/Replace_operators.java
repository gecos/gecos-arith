package fr.citilab.socrate.smallConstantMultiplierDivisor.modules;

import gecos.core.Procedure;
import gecos.core.ProcedureSet;
import gecos.gecosproject.GecosProject;
import fr.citilab.socrate.smallConstantMultiplierDivisor.modules.modifications.Visitor_op_replacer;
import fr.citilab.socrate.smallConstantMultiplierDivisor.modules.util.File_builder;

public class Replace_operators {
	/*
	 * This extension modifies an existing project by substitute multiplication
	 * or division by integer constant with optimized operators for vivado_hls
	 */

	private GecosProject project;
	
	public Replace_operators(GecosProject p) {
		this.project = p;
	}
	
	public GecosProject compute() {
		File_builder.create_ps();
		
		for (ProcedureSet ps : project.listProcedureSets()) { // Iterate on each procedure set contained in a GeCoS project 
			for (Procedure pr : ps.listProcedures()) { // Iterate on each procedure contained in a procedure set
				Visitor_op_replacer visitor = new Visitor_op_replacer();
				pr.getBody().accept(visitor); // apply the visitor on the body block of a procedure
			}
		}
		
		return File_builder.add_files(project);	
	}

}

package fr.irisa.cairn.vivado.hls.sumOfProduct.modules;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.common.util.EList;

import fr.irisa.cairn.gecos.model.analysis.types.TypeAnalyzer;
import fr.irisa.cairn.gecos.model.factory.GecosUserDagFactory;
import fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory;
import fr.irisa.cairn.gecos.model.tools.visitors.GecosBlocksInstructionsDefaultVisitor;
import static fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory.ACINT;
import static fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory.ALIAS;
import static fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory.BOOL;
import static fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory.FIELD;
import static fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory.FLOAT;
import static fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory.UINT;
import static fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory.INT;
import static fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory.UNION;
import static fr.irisa.cairn.gecos.model.factory.GecosUserTypeFactory.setScope;
import static fr.irisa.cairn.gecos.model.factory.GecosUserBlockFactory.BBlock;
import static fr.irisa.cairn.gecos.model.factory.GecosUserBlockFactory.CompositeBlock;
import static fr.irisa.cairn.gecos.model.factory.GecosUserBlockFactory.IfThenElse;
import static fr.irisa.cairn.gecos.model.factory.GecosUserBlockFactory.For;
import static fr.irisa.cairn.gecos.model.factory.GecosUserCoreFactory.paramSymbol;
import static fr.irisa.cairn.gecos.model.factory.GecosUserCoreFactory.proc;
import static fr.irisa.cairn.gecos.model.factory.GecosUserCoreFactory.procSymbol;
import static fr.irisa.cairn.gecos.model.factory.GecosUserCoreFactory.symbol;
import static fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory.Int;
import static fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory.add;
import static fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory.call;
import static fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory.array;
import static fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory.cast;
import static fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory.dummy;
import static fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory.ge;
import static fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory.eq;
import static fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory.lt;
import static fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory.field;
import static fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory.methodCallInstruction;
import static fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory.mul;
import static fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory.neg;
import static fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory.not;
import static fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory.ret;
import static fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory.set;
import static fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory.shr;
import static fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory.shl;
import static fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory.sub;
import static fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory.symbref;
import static fr.irisa.cairn.gecos.model.factory.GecosUserInstructionFactory.xor;
import gecos.annotations.IAnnotation;
import gecos.annotations.PragmaAnnotation;
import gecos.blocks.BasicBlock;
import gecos.blocks.Block;
import gecos.blocks.CompositeBlock;
import gecos.blocks.ForBlock;
import gecos.blocks.IfBlock;
import gecos.core.ParameterSymbol;
import gecos.core.Procedure;
import gecos.core.ProcedureSet;
import gecos.core.ProcedureSymbol;
import gecos.core.Symbol;
import gecos.dag.DAGCallNode;
import gecos.dag.DAGEdge;
import gecos.dag.DAGInPort;
import gecos.dag.DAGInstruction;
import gecos.dag.DAGNode;
import gecos.dag.DAGOpNode;
import gecos.dag.DAGOutPort;
import gecos.dag.DAGSymbolNode;
import gecos.gecosproject.GecosProject;
import gecos.instrs.Instruction;
import gecos.types.AliasType;
import gecos.types.Field;
import gecos.types.RecordType;

/**
 * SumOfProduct is a sample module. When the script evaluator encounters
 * the 'SumOfProduct' function, it calls the compute method.
 */
public class SumOfProduct extends GecosBlocksInstructionsDefaultVisitor{
	
	private GecosProject p;
	String VAR;
	int LSBA, MSBA, MAXMSBX;
	boolean sum_of_product;
	int MANTISSA = 23;
	int EXP = 8;
	int INPUT_SIZE = MANTISSA + EXP +1;
	int FP_BIAS = 127;
	AliasType fp_to_ap;
	ArrayList<DAGNode> mul_nodes;
	ArrayList<DAGNode> regular_nodes;
	boolean env_set;
	DAGNode bottom = null;
	boolean fp_to_acc_added, fp_to_acc_for_mult_added, product_added;
	Symbol long_acc_symb = null;
	boolean jump;
	
	public SumOfProduct (GecosProject p) {
		this.p = p;
		sum_of_product =false;
		fp_to_ap = null;
		mul_nodes = new ArrayList<DAGNode>();
		regular_nodes = new ArrayList<DAGNode>();
		env_set = false;
		fp_to_acc_added = false;
		fp_to_acc_for_mult_added = false;
		product_added = false;
	}
	
	public void compute() {
		System.out.println("SumOfProduct plug-in");
		// Get the list of procedures from the source code.
		List<ProcedureSet> listOfProcedureSets = p.listProcedureSets();


		// Border cases
		if(listOfProcedureSets.size()>1 || listOfProcedureSets.isEmpty()){
			System.out.println("Your program should only contain one ProcedureSet.");
			System.exit(1);
		}

		ProcedureSet procSet = listOfProcedureSets.get(0);
		IAnnotation ap_int_include = procSet.getAnnotation("#pragma");
		if(ap_int_include == null){
			procSet.addPragma("S2S4HLS:MODULE:PRINT:#include <ap_int.h>");
			
		}
		procSet.addPragma("S2S4HLS:MODULE:PRINT:#include \"hint.hpp\"");
		procSet.addPragma("S2S4HLS:MODULE:PRINT:#include \"bitvector/lzoc_shifter.hpp\"");
		procSet.addPragma("S2S4HLS:MODULE:PRINT:#include \"bitvector/shifter_sticky.hpp\"");

		List<Procedure>  listOfProcedures = procSet.listProcedures();


		int procCounter;
		for(procCounter=0; procCounter < listOfProcedures.size(); procCounter++){
			listOfProcedures.get(procCounter).getBody().accept(this);
		}		
	}
	
	public void visitDAGInstruction(DAGInstruction DAG){
		Block tmp = DAG.getBasicBlock();
		boolean apply_transforms = false;
		List<ProcedureSet> listOfProcedureSets = p.listProcedureSets();
		ProcedureSet procSet = listOfProcedureSets.get(0);

		while (tmp!=null){
			if(tmp instanceof ForBlock){
				ForBlock f = (ForBlock) tmp;
				PragmaAnnotation pragma = f.getPragma();
				if (pragma == null){
					tmp = tmp.getParent();
					continue;
				}
				EList<String> objects = pragma.getContent();
				String pragma_string = objects.get(0);
				String[] pragma_parts = pragma_string.toLowerCase().split(" ");
				if(!pragma_parts[0].equals("fpacc")){
					tmp = tmp.getParent();
					continue;
				}		

				apply_transforms = true;

				if(!env_set){
					set_env_varibles(pragma_parts);
					add_new_variables_and_convert_to_fp(f, procSet);
					add_acc_to_fp_proc(procSet);
				}
				break;

			}
			else{
				tmp = tmp.getParent();				
			}
		}

		if(apply_transforms){
			jump = false;
			normalize_DAG(DAG);

			if (!jump){
				apply_acc_and_sum_of_products(DAG, procSet);	
			}

		}


	}

	public void apply_acc_and_sum_of_products(DAGInstruction DAG, ProcedureSet procSet){



		//assert(bottom.getInputs().size()==1);
		DAGNode accumulation_Node = bottom.getInputs().get(1).getSourceNode();
		EList<DAGInPort> accumulation_inputs = accumulation_Node.getInputs();

		assert (accumulation_inputs.size()==2);
		DAGNode first_source = accumulation_inputs.get(0).getSourceNode();
		DAGNode second_source = accumulation_inputs.get(1).getSourceNode();

		DAGNode to_apply_func = null;
		if(first_source.toString().contains("long_accumulator_generated")){
			to_apply_func = second_source;
		}
		else{
			to_apply_func = first_source;
		}



		check_sum_of_product(to_apply_func);


		sum_of_product = (mul_nodes.size()!=0);



		if(sum_of_product){
			if(!fp_to_acc_for_mult_added){
				add_FP_to_accumulable_Procedure(procSet, true);	
				fp_to_acc_for_mult_added = true;	
			}
			if(!product_added){
				add_product_Procedure(procSet);
				product_added = true;
			}
		}
		if(!fp_to_acc_added){
			add_FP_to_accumulable_Procedure(procSet, false);
			fp_to_acc_added = true;
		}

		boolean nodes_to_apply_funcs = (mul_nodes.size()!=0) || (regular_nodes.size()!=0);

		for(int i=0; i<mul_nodes.size(); i++){
			Procedure proc = p.listProcedureSets().get(0).findProcedure("FP_to_accumulable_for_mult");
			ProcedureSymbol psymbol = proc.getSymbol();

			DAGSymbolNode sym_node = GecosUserDagFactory.createDAGSymbolNode(DAG, psymbol);
			DAGCallNode fp_to_acc_call_node = GecosUserDagFactory.createDAGCallNode(DAG, psymbol.getName(), psymbol.getType(), true);
			DAG.connectNodes(sym_node, fp_to_acc_call_node);

			DAG.connectNodes(fp_to_acc_call_node, mul_nodes.get(i).getOutputs().get(0).getSinks().get(0).getSinkNode());

			mul_nodes.get(i).getOutputs().get(0).getSinks().get(0).setSink(GecosUserDagFactory.createDAGInputPort(fp_to_acc_call_node));	

			Procedure proc_mul = p.listProcedureSets().get(0).findProcedure("product");
			ProcedureSymbol psymbol_mul = proc_mul.getSymbol();

			DAGSymbolNode sym_node_mul = GecosUserDagFactory.createDAGSymbolNode(DAG, psymbol_mul);

			DAGCallNode mul_call_node = GecosUserDagFactory.createDAGCallNode(DAG, psymbol_mul.getName(), psymbol_mul.getType(), true);
			DAG.connectNodes(sym_node_mul, mul_call_node);

			DAG.replaceNode(mul_call_node, mul_nodes.get(i));
		}
		mul_nodes.clear();

		for(int i=0; i<regular_nodes.size(); i++){
			Procedure proc = p.listProcedureSets().get(0).findProcedure("FP_to_accumulable");
			ProcedureSymbol psymbol = proc.getSymbol();

			DAGSymbolNode sym_node = GecosUserDagFactory.createDAGSymbolNode(DAG, psymbol);
			DAGCallNode fp_to_acc_call_node = GecosUserDagFactory.createDAGCallNode(DAG, psymbol.getName(), psymbol.getType(), true);
			DAG.connectNodes(sym_node, fp_to_acc_call_node);
			DAG.connectNodes(fp_to_acc_call_node, regular_nodes.get(i).getOutputs().get(0).getSinks().get(0).getSinkNode());

			regular_nodes.get(i).getOutputs().get(0).getSinks().get(0).setSink(GecosUserDagFactory.createDAGInputPort(fp_to_acc_call_node));	


		}
		regular_nodes.clear();

		if(!nodes_to_apply_funcs){
			Procedure proc = p.listProcedureSets().get(0).findProcedure("FP_to_accumulable");
			ProcedureSymbol psymbol = proc.getSymbol();

			DAGSymbolNode sym_node = GecosUserDagFactory.createDAGSymbolNode(DAG, psymbol);
			DAGCallNode fp_to_acc_call_node = GecosUserDagFactory.createDAGCallNode(DAG, psymbol.getName(), psymbol.getType(), true);

			DAG.connectNodes(sym_node, fp_to_acc_call_node);
			DAG.connectNodes(fp_to_acc_call_node, accumulation_Node);

			to_apply_func.getOutputs().get(0).getSinks().get(0).setSink(GecosUserDagFactory.createDAGInputPort(fp_to_acc_call_node));	

		}


	}

	public void normalize_DAG(DAGInstruction DAG){
		//		System.out.println(DAG.toString());

		EList<DAGNode> nodes = DAG.getNodes();
		DAGNode accumulatedNode = null;
		int var_count = 0;
		for (int i = 0; i<nodes.size(); i++){
			DAGNode currentNode = nodes.get(i);
			if(currentNode.toString().contains(VAR.concat(":"))){
				var_count++;
				if(currentNode.getOutputs().size()!=0){
					accumulatedNode = currentNode;
				}
				if(currentNode.getOutputs().size()==0){
					//					System.out.println(currentNode.getPredecessors().get(0).getPredecessors().toString());
					bottom = currentNode;
					//					System.out.println(bottom.toString());
				}
			}
		}
		if(var_count == 0){
			jump=true;
			return;
		}

		assert(var_count==2);
		assert(accumulatedNode!=null);

		// Scheme to transform: 
		//
		//     ?     acc         <-accumulated node
		//      \   /
		//        +              <-upper operator
		//        |  ?
		//        | /
		//        +              <-lower operator
		//		  |
		//
		//
		// Into:
		//
		//     ?     ?
		//      \   /
		//        +
		//        |  acc
		//        | /
		//        +
		//		  |
		//



		while(!accumulatedNode.getSuccessors().get(0).getSuccessors().get(0).equals(bottom)){
			EList<DAGOutPort> output_edges = accumulatedNode.getOutputs();
			assert (output_edges.size()==1);
			assert(output_edges.get(0).getSinks().size() == 1);


			DAGNode upper_operator = output_edges.get(0).getSinks().get(0).getSinkNode();

			if(!upper_operator.toString().contains("add")){
				System.out.println("Proccess FAILED.");
				return; 
			}

			assert(upper_operator.getSuccessors().size()==1);
			DAGNode lower_operator = upper_operator.getOutputs().get(0).getSinks().get(0).getSinkNode();

			if(!lower_operator.toString().contains("add")){
				System.out.println("Proccess FAILED.");
				return; 			
			}

			EList<DAGInPort> lower_operator_inputs = lower_operator.getInputs();
			assert(lower_operator_inputs.size()==2);

			DAGNode lower_first_input = lower_operator_inputs.get(0).getSourceNode();
			DAGNode lower_second_input = lower_operator_inputs.get(1).getSourceNode();

			DAGNode node_to_switch = null;

			if(lower_first_input.equals(upper_operator)){
				node_to_switch = lower_second_input;
			}
			else{
				node_to_switch = lower_first_input;
			}

			assert (node_to_switch.getOutputs().size()==1);
			assert (node_to_switch.getOutputs().get(0).getSinks().size()==1);

			DAGEdge node_to_switch_edge = node_to_switch.getOutputs().get(0).getSinks().get(0);

			assert (accumulatedNode.getOutputs().size()==1);
			assert (accumulatedNode.getOutputs().get(0).getSinks().size()==1);

			DAGEdge accumulatedNode_edge = accumulatedNode.getOutputs().get(0).getSinks().get(0);


			DAGInPort temp = node_to_switch_edge.getSink();
			node_to_switch_edge.setSink(accumulatedNode_edge.getSink());
			accumulatedNode_edge.setSink(temp);


		}

		DAGSymbolNode long_acc_symb_node = GecosUserDagFactory.createDAGSymbolNode(DAG, DAG.getBasicBlock().getScope().lookup("long_accumulator_generated"));
		DAG.replaceNode(long_acc_symb_node, bottom);
		bottom = long_acc_symb_node;		

		DAGSymbolNode long_acc_symb_node_2 = GecosUserDagFactory.createDAGSymbolNode(DAG, DAG.getBasicBlock().getScope().lookup("long_accumulator_generated"));
		if(bottom.getInputs().get(0).getSourceNode().getInputs().get(0).getSourceNode().toString().contains(VAR)){
			DAG.replaceNode(long_acc_symb_node_2, bottom.getInputs().get(0).getSourceNode().getInputs().get(0).getSourceNode());
		}
		else{
			DAG.replaceNode(long_acc_symb_node_2, bottom.getInputs().get(0).getSourceNode().getInputs().get(1).getSourceNode());
		}


		TypeAnalyzer analyzer_1 = new TypeAnalyzer(DAG.getBasicBlock().getScope().lookup("long_accumulator_generated").getType());
		DAGOpNode set_node = GecosUserDagFactory.createDAGOPNode(DAG, "SET", analyzer_1.getBaseLevel().getBase());
		DAG.connectNodes(long_acc_symb_node, set_node);
		GecosUserDagFactory.createDAGInputPort(set_node);
		long_acc_symb_node.getInputs().get(0).getSourceNode().getOutputs().get(0).getSinks().get(0).setSink(set_node.getInputs().get(1));
		//		to_apply_func.getOutputs().get(0).getSinks().get(0).setSink(GecosUserDagFactory.createDAGInputPort(set_node));


		bottom = set_node;

	}


	public void add_new_variables_and_convert_to_fp(ForBlock f, ProcedureSet procSet){

		

		// Add structure for float to bits conversion
		setScope(procSet.getScope());
		Field fField = FIELD("f", FLOAT());
		Field apField = FIELD("ap",UINT());
		fp_to_ap = ALIAS(
				UNION("float_to_ap_int", 
						fField,
						apField)
				, "bitwise_float_to_ap_int");


		// Initialize (source code) variables for loop transformation
		Block parent_block =  f.getParent();
		assert (parent_block instanceof CompositeBlock);
		CompositeBlock parent_composite_block = (CompositeBlock) parent_block;


		setScope(parent_composite_block.getScope());

		long_acc_symb = symbol("long_accumulator_generated", ACINT(MSBA-LSBA+1, false));
		parent_composite_block.addSymbol(long_acc_symb);



		BasicBlock initialisation_of_acc_variable = BBlock(set(long_acc_symb, 0));
		parent_composite_block.addBlockBefore(initialisation_of_acc_variable, f);

		// Convert returned value from large fixed to float
		Symbol var_symbol = parent_composite_block.getScope().lookup(VAR);
		assert(var_symbol!=null);

		ArrayList<ParameterSymbol> acc_to_fp_param = new ArrayList<ParameterSymbol>();
		acc_to_fp_param.add(paramSymbol("long_accumulator_generated",  ACINT(MSBA-LSBA+1, false)));
		ProcedureSymbol acc_to_fp_symbol = procSymbol("acc_to_fp", FLOAT(), acc_to_fp_param);
		BasicBlock result_reconstruction = BBlock(
				set(symbref(var_symbol), call(symbref(acc_to_fp_symbol), symbref(long_acc_symb)))
				);
		parent_composite_block.addBlockAfter(result_reconstruction, f);





		// Find use of variable to make the transformation
		CompositeBlock loop_body = (CompositeBlock) f.getBodyBlock();
		EList<Block> body_blocks = loop_body.getChildren();	
		body_blocks.get(0).addPragma("HLS PIPELINE II=1");
	}

	public void set_env_varibles(String[] pragma_parts){
		boolean[] all_params = {false, false, false, false};

		for(int i=1; i<pragma_parts.length; i++){
			String current_item = pragma_parts[i];
			String[] key_value = current_item.split("=");
			assert(key_value.length==2);
			if(key_value[0].equals("var")){
				VAR = key_value[1];
				all_params[0] =true;
			}
			else if(key_value[0].equals("epsilon")){
				//LSBA = (int) Math.ceil(Math.log(Math.pow(10.0, (double) Integer.parseInt(key_value[1])))/Math.log(2));
				try {
					LSBA = (int) Math.floor(log2((double) Double.parseDouble(key_value[1])));
				}
				catch(NumberFormatException e){
					System.err.println("Pragma error : Floating point values have to be written with a dot (\".\") and not a comma (\",\").");
					System.err.println("eg. 1,751 is not a valid input but 1.751 is.");
					System.err.println("epsilon.");
					System.exit(0);
				}
				all_params[1] =true;
			}
			else if(key_value[0].equals("maxacc")){
				try {
					MSBA = (int) Math.ceil(log2((double) Double.parseDouble(key_value[1])));
				}
				catch(NumberFormatException e){
					System.err.println("Pragma error : Floating point values have to be written with a dot (\".\") and not a comma (\",\").");
					System.err.println("eg. 1,751 is not a valid input but 1.751 is.");
					System.err.println("MaxAcc.");
					System.exit(0);
				}
				//MSBA = (int) Math.ceil(Math.log(Math.pow(10.0, (double) Integer.parseInt(key_value[1])))/Math.log(2));

				all_params[2] =true;
			}
			else if(key_value[0].equals("maxinput")){
				try {
					MAXMSBX = (int) Math.ceil(log2((double) Double.parseDouble(key_value[1])))+1;
				}
				catch(NumberFormatException e){
					System.err.println("Pragma error : Floating point values have to be written with a dot (\".\") and not a comma (\",\").");
					System.err.println("eg. 1,751 is not a valid input but 1.751 is.");
					System.err.println("MaxInput.");
					System.exit(0);
				}
				//				MAXMSBX = (int) Math.ceil(Math.log(Math.pow(10.0, (double) Integer.parseInt(key_value[1])))/Math.log(2));
				all_params[3] =true;
			}
			else{
				System.err.println("Pragma error, unknown " + key_value[0] + " parameter.");
				System.exit(0);
			}
		}

		for (int i=0; i<4; i++){
			if(!all_params[i] && i==3){
				all_params[i] = true;
				MAXMSBX = MSBA;
			}
			if(!all_params[i]){
				System.err.println("Use pragma with parameters :");
				System.err.println("VAR=xxx : Accumulation variable");
				System.err.println("epsilon=xxx : Desired accuracy");
				System.err.println("MSBA=xxx : Maximum accumulation value");
				System.err.println("MAXSBX=xxx : Maximum summand value");
				System.exit(0);;
			}
		}
		if (MAXMSBX>MSBA){
			MAXMSBX=MSBA;
		}
		System.out.println("SumOfProduct called on variable " + VAR);
		System.out.println("With " + MSBA + " bits for the decimal part and " + -LSBA + " bits for the fractionnal part.");
		System.out.println("MAXMSBX is set to " + MAXMSBX + ".");
		env_set = true;
	}

	public void check_sum_of_product(DAGNode node){

		if(node.toString().contains("add")){
			DAGNode left_node = node.getInputs().get(0).getSourceNode();
			DAGNode right_node = node.getInputs().get(1).getSourceNode();

			boolean left_mul = false, left_add = false, right_mul = false, right_add = false;
			// Check mul nodes
			if(left_node.toString().contains("mul")){
				mul_nodes.add(left_node);
				left_mul = true;
			}
			if(right_node.toString().contains("mul")){
				mul_nodes.add(right_node);
				right_mul = true;
			}

			// Check add nodes
			if(left_node.toString().contains("add")){
				check_sum_of_product(left_node);
				left_add = true;
			}
			if(right_node.toString().contains("add")){
				check_sum_of_product(right_node);
				right_add = true;
			}


			if(!left_mul && !left_add){
				regular_nodes.add(left_node);
			}
			if(!right_mul && !right_add){
				regular_nodes.add(right_node);
			}
		}
		else if(node.toString().contains("mul")){
			mul_nodes.add((node));
		}

	}

	public void add_acc_to_fp_proc(ProcedureSet procSet) {

		setScope(procSet.getScope());
		CompositeBlock mainblock = CompositeBlock();
		CompositeBlock outerblock = CompositeBlock();
		outerblock.addChildren(mainblock);
		setScope(mainblock.getScope());	
	
		
		ArrayList<ParameterSymbol> param_symbols= new ArrayList<ParameterSymbol>();
		param_symbols.add(paramSymbol("long_accumulator_generated",  ACINT(MSBA-LSBA+1, false)));
		ProcedureSymbol psymbol = procSymbol("acc_to_fp", FLOAT(), param_symbols);
		proc(procSet, psymbol, outerblock);
		mainblock.addPragma("HLS INLINE");
		
		Symbol s = symbol("s", ACINT(1, false));
		Symbol exp = symbol("exp", ACINT(EXP, false));
		Symbol mant = symbol("mant", ACINT(MANTISSA, false));
		Symbol ret = symbol("ret", ACINT(MANTISSA + EXP + 1, false));
		Symbol ret_without_sign = symbol("ret_without_sign", ACINT(MANTISSA + EXP, false));
		Symbol bits_to_fp = symbol("bits_to_fp", fp_to_ap);
		Symbol lzoc_shift = symbol("lzoc_shift", ACINT(MSBA-LSBA+1+(int)Math.ceil(log2(MSBA-LSBA+1)), false));
		Symbol lzoc = symbol("lzoc", ACINT((int)Math.ceil(log2(MSBA-LSBA+1)), false));
		Symbol mask = symbol("mask", ACINT(MANTISSA, false));
		
		mainblock.addSymbol(s);
		mainblock.addSymbol(exp);
		mainblock.addSymbol(mant);
		mainblock.addSymbol(ret);
		mainblock.addSymbol(ret_without_sign);
		mainblock.addSymbol(bits_to_fp);
		mainblock.addSymbol(lzoc_shift);
		mainblock.addSymbol(lzoc);
		mainblock.addSymbol(mask);
		
		BasicBlock set_s = BBlock(
				set(symbref(s),array(symbref(param_symbols.get(0)), Int(MSBA-LSBA)))
			);
		mainblock.addChildren(set_s);
		
		ArrayList<ParameterSymbol> generic_lzoc_shifter_param_symbols = new ArrayList<ParameterSymbol>();
		generic_lzoc_shifter_param_symbols.add(paramSymbol("to_shift_and_lzoc",  ACINT(MSBA-LSBA+1, false)));
		generic_lzoc_shifter_param_symbols.add(paramSymbol("zo",  ACINT(1, false)));
		ProcedureSymbol generic_lzoc_shifter_symbol = procSymbol("generic_lzoc_shifter", ACINT(MSBA-LSBA+1+(int)Math.ceil(log2(MSBA-LSBA+1)), false), generic_lzoc_shifter_param_symbols);
		BasicBlock set_lzoc_shift = BBlock(
				set(symbref(lzoc_shift), call(symbref(generic_lzoc_shifter_symbol), symbref(param_symbols.get(0)), array(symbref(param_symbols.get(0)), Int(MSBA-LSBA))))
				);
		mainblock.addChildren(set_lzoc_shift);
		
		ArrayList<Instruction> lzoc_range_params = new ArrayList<Instruction>();
		lzoc_range_params.add(0, Int(MSBA-LSBA+1+(int)Math.ceil(log2(MSBA-LSBA+1)-1)));
		lzoc_range_params.add(1, Int(MSBA-LSBA+1));
		BasicBlock set_lzoc = BBlock(
				set(lzoc, methodCallInstruction("range", symbref(lzoc_shift), lzoc_range_params)));
		mainblock.addChildren(set_lzoc);
		
		ArrayList<Instruction> mant_range_params = new ArrayList<Instruction>();
		mant_range_params.add(0, Int(MSBA-LSBA+1-2));
		mant_range_params.add(1, Int(MSBA-LSBA+1-2-MANTISSA+1));
		BasicBlock set_mant = BBlock(
				set(mant, methodCallInstruction("range", symbref(lzoc_shift), mant_range_params)));
		mainblock.addChildren(set_mant);
		
		Symbol i = symbol("i", INT());
		mainblock.addSymbol(i);
		BasicBlock init_block = BBlock(set(symbref(i), Int(0)));
		BasicBlock test_block = BBlock(lt(symbref(i), Int(MANTISSA)));
		BasicBlock step_block = BBlock(set(symbref(i), add(symbref(i), Int(1))));
		BasicBlock body_block = BBlock(set(array(mask, symbref(i)), symbref(s)));
		CompositeBlock outer_loopblock = CompositeBlock();
		outer_loopblock.addChildren(body_block);
		body_block.addPragma("HLS UNROLL");
		ForBlock maskloop = For(init_block, test_block, step_block, outer_loopblock);
		mainblock.addChildren(maskloop);	
		
		BasicBlock set_mant_xored = BBlock(
				set(symbref(mant), add(xor(symbref(mant), symbref(mask)), symbref(s)))
				);
		mainblock.addChildren(set_mant_xored);

		BasicBlock set_exp = BBlock(
				set(symbref(exp), sub(Int(MSBA-1+FP_BIAS), symbref(lzoc)))
				);
		mainblock.addChildren(set_exp);
		
		Field fField = FIELD("f", FLOAT());
		Field apField = FIELD("ap",UINT());
		BasicBlock result_reconstruction  = BBlock(
				set(symbref(ret_without_sign), methodCallInstruction("concat", symbref(exp), symbref(mant))),
				set(symbref(ret), methodCallInstruction("concat", symbref(s), symbref(ret_without_sign))),
				set(field(symbref(bits_to_fp), apField), symbref(ret)),
				ret(field(symbref(bits_to_fp), fField))
				);
		mainblock.addChildren(result_reconstruction);
		
		
	}
	
	
	public void add_FP_to_accumulable_Procedure(ProcedureSet procSet, boolean for_mult){
		setScope(procSet.getScope());

		ArrayList<ParameterSymbol> param_symbols= new ArrayList<ParameterSymbol>();
		ProcedureSymbol psymbol = null;
		CompositeBlock mainblock = CompositeBlock();
		CompositeBlock outerblock = CompositeBlock();
		outerblock.addChildren(mainblock);
		
		setScope(mainblock.getScope());	
		Symbol in = null;
		Symbol var = null;

		
		if(for_mult){
			param_symbols.add(paramSymbol("in",  ACINT(2*MANTISSA + 2 + EXP + 1 + 1, false)));
			psymbol = procSymbol("FP_to_accumulable_for_mult", ACINT(MSBA-LSBA+1, false), param_symbols);
		}
		else{
			param_symbols.add(paramSymbol("a",  FLOAT()));
			psymbol = procSymbol("FP_to_accumulable", ACINT(MSBA-LSBA+1, false), param_symbols);

			in = symbol("in", ACINT(INPUT_SIZE, false));
			var = symbol("var", fp_to_ap);
			mainblock.addSymbol(in);
			mainblock.addSymbol(var);

			TypeAnalyzer analyzer_1 = new TypeAnalyzer(var.getType());
			if(!analyzer_1.getBaseLevel().isRecord()) 
				throw new RuntimeException("Type of " + var + " is not a RecordType");


			RecordType record_1 = (RecordType) analyzer_1.getBaseLevel().getBase();
			Field apField_1;
			if((apField_1 = record_1.findField("f")) == null)
				throw new RuntimeException("F field not find in " + record_1.getName());

			TypeAnalyzer analyzer_2 = new TypeAnalyzer(var.getType());
			if(!analyzer_2.getBaseLevel().isRecord()) 
				throw new RuntimeException("Type of " + var + " is not a RecordType");


			RecordType record_2 = (RecordType) analyzer_2.getBaseLevel().getBase();
			Field apField_2;
			if((apField_2 = record_2.findField("ap")) == null)
				throw new RuntimeException("Ap field not find in " + record_2.getName());


			BasicBlock conversion = BBlock(
					set(field(symbref(var), apField_1), symbref(param_symbols.get(0))),
					set(symbref(in), field(symbref(var), apField_2))
					);
			mainblock.addChildren(conversion);
		}

		mainblock.addPragma("HLS INLINE");

		proc(procSet, psymbol, outerblock);


		Symbol exp_u = null;
		Symbol mantissa = null;
		Symbol smantissa = null;
		Symbol shifted = null;
		Symbol to_sum = null;
		Symbol mask = null;
		Symbol ext_to_sum = symbol("ext_to_sum", ACINT(MSBA-LSBA+1, true));
		Symbol left_shifter_arg1 = null;
		Symbol left_shifter_arg2 = null;

		
		Symbol sign = symbol("sign", ACINT(1, false));
		if(!for_mult){
			exp_u = symbol("exp_u", ACINT(EXP, false));
			mantissa = symbol("mantissa", ACINT(MANTISSA +1, false));
			smantissa = symbol("smantissa", ACINT(MANTISSA +1+1, true));
			shifted = symbol("shifted", ACINT(MAXMSBX-LSBA+2+MANTISSA+1, false));
			to_sum = symbol("to_sum", ACINT(MAXMSBX-LSBA+2, true));
			mask = symbol("mask", ACINT(MANTISSA + 2 , false));
			left_shifter_arg1 = symbol("left_shifter_arg1", ACINT(MAXMSBX-LSBA+2+MANTISSA, false));
			left_shifter_arg2 = symbol("left_shifter_arg2", ACINT(EXP, false));
		}
		else{
			exp_u = symbol("exp_u", ACINT(EXP+1, false));
			mantissa = symbol("mantissa", ACINT(2*MANTISSA +2, false));
			to_sum = symbol("to_sum", ACINT(MAXMSBX-LSBA+1, true));
			mask = symbol("mask", ACINT(MAXMSBX-LSBA+1, true));
			left_shifter_arg1 = symbol("left_shifter_arg1", ACINT(MAXMSBX-LSBA+1+2*MANTISSA+2, false));
			left_shifter_arg2 = symbol("left_shifter_arg2", ACINT(EXP+1, false));
			shifted = symbol("shifted", ACINT(MAXMSBX-LSBA+1+2*MANTISSA+2, false));
		}
		
		mainblock.addSymbol(exp_u);
		mainblock.addSymbol(mantissa);
		mainblock.addSymbol(sign);
		mainblock.addSymbol(ext_to_sum);
		mainblock.addSymbol(shifted);
		mainblock.addSymbol(to_sum);
		mainblock.addSymbol(mask);
		mainblock.addSymbol(left_shifter_arg1);
		mainblock.addSymbol(left_shifter_arg2);
		mainblock.addSymbol(ext_to_sum);
		
		if(!for_mult){
			mainblock.addSymbol(smantissa);
		}		

		ArrayList<Instruction> mantissa_range_params = new ArrayList<Instruction>();
		if(!for_mult){
			mantissa_range_params.add(0, Int(MANTISSA-1));
		}
		else{
			mantissa_range_params.add(0, Int(2*MANTISSA+1));
		}
		mantissa_range_params.add(1, Int(0));

		BasicBlock get_mantissa = null;
		if(!for_mult){
			get_mantissa = BBlock(
					set(mantissa, methodCallInstruction("range", symbref(in), mantissa_range_params)));	
		}
		else{
			get_mantissa = BBlock(
					set(mantissa, methodCallInstruction("range", symbref(param_symbols.get(0)), mantissa_range_params)));
		}
		mainblock.addChildren(get_mantissa);

		if(!for_mult){
			ArrayList<Instruction> exp_u_range_params = new ArrayList<Instruction>();
			exp_u_range_params.add(0, Int(MANTISSA+EXP-1));
			exp_u_range_params.add(1, Int(MANTISSA));

			BasicBlock get_exp_u = BBlock(
					set(exp_u, methodCallInstruction("range", symbref(in), exp_u_range_params)));

			mainblock.addChildren(get_exp_u);
			BasicBlock complete_mantissa = BBlock(	
					set(array(mantissa, Int(MANTISSA)),  methodCallInstruction("or_reduce", symbref(exp_u))));

			mainblock.addChildren(complete_mantissa);
		}
		else{
			ArrayList<Instruction> exp_u_range_params = new ArrayList<Instruction>();
			exp_u_range_params.add(0, Int(2*MANTISSA+2+EXP));
			exp_u_range_params.add(1, Int(2*MANTISSA+2));

			BasicBlock get_exp_u = BBlock(
					set(exp_u, methodCallInstruction("range", symbref(param_symbols.get(0)), exp_u_range_params)));

			mainblock.addChildren(get_exp_u);
		}


		BasicBlock get_s;
		if(!for_mult){
			get_s = BBlock(set(symbref(sign) , array(symbref(in), Int(MANTISSA+EXP))));
		}
		else{
			get_s = BBlock(set(symbref(sign) , array(symbref(param_symbols.get(0)), Int(2*MANTISSA+2+EXP+1))));
		}
		mainblock.addChildren(get_s);
	

		
		if(for_mult) {
			BasicBlock set_left_shifter_arg1 = BBlock(set(symbref(left_shifter_arg1), symbref(mantissa)));
			BasicBlock set_left_shifter_arg2 = BBlock(set(symbref(left_shifter_arg2), add(symbref(exp_u), Int(-LSBA+1+1))));
			mainblock.addChildren(set_left_shifter_arg1);
			mainblock.addChildren(set_left_shifter_arg2);
			
			ArrayList<ParameterSymbol> left_shifter_param_symbols = new ArrayList<ParameterSymbol>();
			left_shifter_param_symbols.add(paramSymbol("to_shift",  ACINT(MAXMSBX-LSBA+1+2*MANTISSA+2, false)));
			left_shifter_param_symbols.add(paramSymbol("by_shift",  ACINT(EXP+1, false)));
			ProcedureSymbol left_shifter_symbol = procSymbol("left_shifter", ACINT(MAXMSBX-LSBA+1+2*MANTISSA+2+1, false), left_shifter_param_symbols);
			
			BasicBlock set_shifted = BBlock(
					set(symbref(shifted), call(symbref(left_shifter_symbol), symbref(left_shifter_arg1), symbref(left_shifter_arg2)))
					);
			mainblock.addChildren(set_shifted);

			
			ArrayList<Instruction> shifted_range_params = new ArrayList<Instruction>();
			shifted_range_params.add(0, Int(MAXMSBX-LSBA+1+2*MANTISSA+2-1));
			shifted_range_params.add(1, Int(2*MANTISSA+2));
			
			BasicBlock set_current_shifted_value = BBlock(
					set(symbref(to_sum), methodCallInstruction("range", symbref(shifted), shifted_range_params))
					);
			mainblock.addChildren(set_current_shifted_value);
			Symbol i = symbol("i", INT());
			mainblock.addSymbol(i);
			BasicBlock init_block = BBlock(set(symbref(i), Int(0)));
			BasicBlock test_block = BBlock(lt(symbref(i), Int(MAXMSBX-LSBA+1)));
			BasicBlock step_block = BBlock(set(symbref(i), add(symbref(i), Int(1))));
			BasicBlock body_block = BBlock(set(array(mask, symbref(i)), symbref(sign)));
			CompositeBlock outer_loopblock = CompositeBlock();
			outer_loopblock.addChildren(body_block);
			body_block.addPragma("HLS UNROLL");
			
			ForBlock maskloop = For(init_block, test_block, step_block, outer_loopblock);
			mainblock.addChildren(maskloop);	

			
			BasicBlock set_to_sum_ext = BBlock(
					set(symbref(ext_to_sum),add(xor(symbref(to_sum), symbref(mask)), symbref(sign)))
				);
			mainblock.addChildren(set_to_sum_ext);
			
			BasicBlock ret_block = BBlock(
					ret(symbref(ext_to_sum))
				);
			mainblock.addChildren(ret_block);
		}
		else {
			
			Symbol i = symbol("i", INT());
			mainblock.addSymbol(i);
			BasicBlock init_block = BBlock(set(symbref(i), Int(0)));
			BasicBlock test_block = BBlock(lt(symbref(i), Int(MANTISSA+2)));
			BasicBlock step_block = BBlock(set(symbref(i), add(symbref(i), Int(1))));
			BasicBlock body_block = BBlock(set(array(mask, symbref(i)), symbref(sign)));
			CompositeBlock outer_loopblock = CompositeBlock();
			outer_loopblock.addChildren(body_block);
			body_block.addPragma("HLS UNROLL");
			
			ForBlock maskloop = For(init_block, test_block, step_block, outer_loopblock);
			mainblock.addChildren(maskloop);	
			
			BasicBlock set_smantissa = BBlock(
					set(symbref(smantissa), add(xor(symbref(mantissa), symbref(mask)), symbref(sign)))
					);
			mainblock.addChildren(set_smantissa);
			
			BasicBlock set_left_shifter_arg1 = BBlock(set(symbref(left_shifter_arg1), symbref(smantissa)));
			BasicBlock set_left_shifter_arg2 = BBlock(set(symbref(left_shifter_arg2), add(symbref(exp_u), Int(-FP_BIAS-LSBA+1))));
			mainblock.addChildren(set_left_shifter_arg1);
			mainblock.addChildren(set_left_shifter_arg2);
			
			ArrayList<ParameterSymbol> left_shifter_param_symbols = new ArrayList<ParameterSymbol>();
			left_shifter_param_symbols.add(paramSymbol("to_shift",  ACINT(2*MANTISSA + 2 + EXP + 1 + 1 + MANTISSA, false)));
			left_shifter_param_symbols.add(paramSymbol("by_shift",  ACINT(EXP, false)));
			ProcedureSymbol left_shifter_symbol = procSymbol("left_shifter", ACINT(2*MANTISSA + 2 + EXP + 1 + 1 + MANTISSA+1, false), left_shifter_param_symbols);
			
			
			BasicBlock set_shifted = BBlock(
					set(symbref(shifted), call(symbref(left_shifter_symbol), symbref(left_shifter_arg1), symbref(left_shifter_arg2)))
					);
			mainblock.addChildren(set_shifted);
					
			ArrayList<Instruction> shifted_range_params = new ArrayList<Instruction>();
			shifted_range_params.add(0, Int(MAXMSBX-LSBA+2+ MANTISSA-1+1));
			shifted_range_params.add(1, Int(MANTISSA+1));
			BasicBlock set_to_sum = BBlock(
					set(symbref(to_sum),methodCallInstruction("range", symbref(shifted), shifted_range_params))
				);
			mainblock.addChildren(set_to_sum);
			BasicBlock set_to_sum_ext = BBlock(
					set(symbref(ext_to_sum),symbref(to_sum))
				);
			mainblock.addChildren(set_to_sum_ext);
			BasicBlock ret_block = BBlock(
					ret(symbref(ext_to_sum))
				);
			mainblock.addChildren(ret_block);
		}
		
		
		

	}

	public void add_product_Procedure(ProcedureSet procSet){
		
		setScope(procSet.getScope());
		ArrayList<ParameterSymbol> param_symbols= new ArrayList<ParameterSymbol>();

		param_symbols.add(paramSymbol("in1",  FLOAT()));
		param_symbols.add(paramSymbol("in2",  FLOAT()));
		ProcedureSymbol psymbol = procSymbol("product", ACINT(2*MANTISSA + 2 + EXP + 1 + 1, false), param_symbols);
		CompositeBlock mainblock = CompositeBlock();
		CompositeBlock outerblock = CompositeBlock();
		outerblock.addChildren(mainblock);
		proc(procSet, psymbol, outerblock);
		setScope(mainblock.getScope());
		mainblock.addPragma("HLS INLINE");
		
		Symbol exp_u_a;
		Symbol exp_u_b;
		Symbol mantissa_a;
		Symbol mantissa_b;
		Symbol zero;
		Symbol var1;
		Symbol var2;
		Symbol a;
		Symbol b;

		var1 = symbol("var1", fp_to_ap);
		var2 = symbol("var2", fp_to_ap);
		a = symbol("a", ACINT(EXP+MANTISSA+1, false));
		b = symbol("b", ACINT(EXP+MANTISSA+1, false));
		exp_u_a = symbol("exp_a", ACINT(EXP, false));
		exp_u_b = symbol("exp_b", ACINT(EXP, false));
		mantissa_a = symbol("m_a", ACINT(MANTISSA+1, false));
		mantissa_b = symbol("m_b", ACINT(MANTISSA+1, false));
		
		mainblock.addSymbol(var1);
		mainblock.addSymbol(var2);	
		mainblock.addSymbol(a);
		mainblock.addSymbol(b);	
		mainblock.addSymbol(exp_u_a);
		mainblock.addSymbol(exp_u_b);
		mainblock.addSymbol(mantissa_a);
		mainblock.addSymbol(mantissa_b);
		

		TypeAnalyzer analyzer_1 = new TypeAnalyzer(var1.getType());
		if(!analyzer_1.getBaseLevel().isRecord()) 
			throw new RuntimeException("Type of " + var1 + " is not a RecordType");


		RecordType record_1 = (RecordType) analyzer_1.getBaseLevel().getBase();
		Field apField_1;
		if((apField_1 = record_1.findField("f")) == null)
			throw new RuntimeException("F field not find in " + record_1.getName());

		TypeAnalyzer analyzer_2 = new TypeAnalyzer(var1.getType());
		if(!analyzer_2.getBaseLevel().isRecord()) 
			throw new RuntimeException("Type of " + var1 + " is not a RecordType");


		RecordType record_2 = (RecordType) analyzer_2.getBaseLevel().getBase();
		Field apField_2;
		if((apField_2 = record_2.findField("ap")) == null)
			throw new RuntimeException("Ap field not find in " + record_2.getName());


		BasicBlock conversion = BBlock(
				set(field(symbref(var1), apField_1), symbref(param_symbols.get(0))),
				set(field(symbref(var2), apField_1), symbref(param_symbols.get(1))),
				set(symbref(a), field(symbref(var1), apField_2)),
				set(symbref(b), field(symbref(var2), apField_2))
				);
		mainblock.addChildren(conversion);

		ArrayList<Instruction> exp_range_params_a = new ArrayList<Instruction>();
		exp_range_params_a.add(0, Int(MANTISSA+EXP-1));
		exp_range_params_a.add(1, Int(MANTISSA));

		ArrayList<Instruction> mantissas_range_params_a = new ArrayList<Instruction>();
		mantissas_range_params_a.add(0, Int(MANTISSA-1));
		mantissas_range_params_a.add(1, Int(0));

		ArrayList<Instruction> exp_range_params_b = new ArrayList<Instruction>();
		exp_range_params_b.add(0, Int(MANTISSA+EXP-1));
		exp_range_params_b.add(1, Int(MANTISSA));

		ArrayList<Instruction> mantissas_range_params_b = new ArrayList<Instruction>();
		mantissas_range_params_b.add(0, Int(MANTISSA-1));
		mantissas_range_params_b.add(1, Int(0));


		BasicBlock get_exps_and_mants_a = BBlock(
				set(exp_u_a, methodCallInstruction("range", symbref(a), exp_range_params_a)),
				set(mantissa_a, methodCallInstruction("range", symbref(a), mantissas_range_params_a))
				);
		BasicBlock get_exps_and_mants_b = BBlock(
				set(exp_u_b, methodCallInstruction("range", symbref(b), exp_range_params_b)),
				set(mantissa_b, methodCallInstruction("range", symbref(b), mantissas_range_params_b))
				);



		mainblock.addChildren(get_exps_and_mants_a);		
		mainblock.addChildren(get_exps_and_mants_b);


		BasicBlock set_mantissas_extra_bit = BBlock(
				set(array(mantissa_a, Int(MANTISSA)), methodCallInstruction("or_reduce", symbref(exp_u_a))),
				set(array(mantissa_b, Int(MANTISSA)), methodCallInstruction("or_reduce", symbref(exp_u_b)))
				);
		mainblock.addChildren(set_mantissas_extra_bit);

		Symbol s_a;
		Symbol s_b;
		Symbol ret_s;

		s_a = symbol("s_a", ACINT(1, false));
		s_b = symbol("s_b", ACINT(1, false));
		ret_s = symbol("ret_s", ACINT(1, false));

		mainblock.addSymbol(s_a);
		mainblock.addSymbol(s_b);
		mainblock.addSymbol(ret_s);

		BasicBlock set_signs = BBlock(
				set(s_a, array(symbref(a), Int(MANTISSA+EXP))),
				set(s_b, array(symbref(b), Int(MANTISSA+EXP))),
				set(ret_s, xor(symbref(s_a), symbref(s_b)))
				); 
		mainblock.addChildren(set_signs);

		Symbol ret_exp_ext;
		Symbol m_product;

		ret_exp_ext = symbol("ret_exp_ext", ACINT(EXP+1, true));
		m_product = symbol("m_product", ACINT(2*MANTISSA+2, false));

		mainblock.addSymbol(ret_exp_ext);
		mainblock.addSymbol(m_product);


		BasicBlock product_Block = BBlock(
				set(m_product, mul(
						cast(ACINT(2*MANTISSA+2, false),  symbref(mantissa_a)), 
						cast(ACINT(2*MANTISSA+2, false),  symbref(mantissa_b))
						)),
				set(ret_exp_ext,
						sub(add(symbref(exp_u_a), symbref(exp_u_b)), Int(2*FP_BIAS))
						)
				);

		mainblock.addChildren(product_Block);

		Symbol ret_without_sign;
		Symbol ret;

		ret_without_sign = symbol("ret_without_sign", ACINT(2*MANTISSA+2+EXP+1, false));
		ret = symbol("ret", ACINT(2*MANTISSA+2+EXP+1+1, false));

		mainblock.addSymbol(ret_without_sign);
		mainblock.addSymbol(ret);

		BasicBlock set_result = BBlock(
				set(ret_without_sign, methodCallInstruction("concat", symbref(ret_exp_ext), symbref(m_product))),
				set(ret, methodCallInstruction("concat", symbref(ret_s), symbref(ret_without_sign))),
				ret(symbref(ret))
				); 
		mainblock.addChildren(set_result);

	}

	double log2(double x) {
		return Math.log(x)/Math.log(2.0d);
	}	
	
}
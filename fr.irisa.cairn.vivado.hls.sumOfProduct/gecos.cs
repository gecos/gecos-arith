debug(1);

project_name = "SumOfProduct";
source = "src-c/exemple.cpp";
CDTUseGecosStandardIncludes();
 
#create project with CreateGecosProject
project = CreateGecosProject(project_name);
#add source folder to project with AddSourceToGecosProject
AddSourceToGecosProject(project,source);


 #build project with GecosProjectBuilder
CDTFrontend(project);

SaveGecosProject(project,"xmi/before_transforms.gecosproject");



GecosTreeToDAGIRConversion(project);
output(project,"dot","./dotty/DAG_before");


SumOfProduct(project);


ClearControlFlow(project);
BuildControlFlow(project);
output(project,"dot","./dotty/DAG_after");
GecosDAGToTreeIRConversion(project);


SetBitAccurateBackend("VivadoAP");

CGenerator(project,"regen/");
output(project,"dot","./dotty/original_tree");
#save the project as XML with SaveGecosProject, currently not working
#SaveGecosProject(project,"xmi/projects.gecosproject");

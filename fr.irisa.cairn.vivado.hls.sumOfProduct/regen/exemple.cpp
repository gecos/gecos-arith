#include <ap_int.h>

union float_to_ap_int {
	float f;
	unsigned int ap;
};
typedef union float_to_ap_int bitwise_float_to_ap_int;
float computeSum(float in1[100000], float in2[100000]);
ap_int<18> FP_to_accumulable(float a);


float computeSum(float in1[100000], float in2[100000]) {
	float sum = 0;
	ap_int<34> long_accumulator_generated;
	ap_uint<1> s;
	ap_uint<8> exp;
	ap_uint<23> mant;
	ap_uint<32> ret;
	ap_uint<31> ret_without_sign;
	unsigned int lzc;
	bitwise_float_to_ap_int bits_to_fp;
	int sh_lzc;
	
	long_accumulator_generated = 0;
	#pragma FPacc VAR=sum MaxAcc=300000 epsilon=1e-4 MaxInput=3
	for(int i = 1; i <= 100000 - 1 - 1; i = i + 1) {
		#pragma HLS PIPELINE II=1
		{
		long_accumulator_generated = FP_to_accumulable(in1[i] + in1[i] * in2[i - 1]) + FP_to_accumulable(in2[i + 1]) + long_accumulator_generated;
		}
	}
	if (long_accumulator_generated[33] == 1) {
		long_accumulator_generated = -long_accumulator_generated;
		s = 1;
	} else
		s = 0;
	lzc = long_accumulator_generated.countLeadingZeros();
	exp = 145 - lzc;
	sh_lzc = 10 - lzc;
	if (sh_lzc >= 0)
		mant = long_accumulator_generated >> sh_lzc;
	else {
		mant = long_accumulator_generated;
		mant = mant << -sh_lzc;
	}
	ret_without_sign = exp.concat(mant);
	ret = s.concat(ret_without_sign);
	bits_to_fp.ap = ret;
	sum = bits_to_fp.f;
	return sum;
}

ap_int<18> FP_to_accumulable(float a) {
	ap_uint<32> in;
	bitwise_float_to_ap_int var;
	ap_uint<8> exp_u;
	ap_int<8> exp_s;
	ap_uint<24> mantissa;
	ap_uint<1> sign;
	ap_int<18> current_shifted_value;
	unsigned int shift_val;
	ap_uint<18> shift_buffer;
	
	var.f = a;
	in = var.ap;
	mantissa = in.range(22, 0);
	mantissa[23] = 1;
	exp_u = in.range(30, 23);
	sign = in[31];
	exp_s = (ap_int<8>)exp_u - 127;
	shift_buffer = mantissa >> 6;
	shift_val = 2 - exp_s;
	current_shifted_value = shift_buffer >> shift_val;
	if (sign == 1)
		return ~current_shifted_value + 1;
	else
		return current_shifted_value;
}


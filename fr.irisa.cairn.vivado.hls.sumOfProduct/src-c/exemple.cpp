#define N 100000
float computeSum(float in1[N], float in2[N]){
	float sum = 0;
	#pragma FPacc VAR=sum MaxAcc=300000 epsilon=1e-4 MaxInput=3
	for (int i=1; i<N-1; i++){
		sum+=in1[i]*in2[i-1];
		sum+=in1[i];
		sum+=in2[i+1];
	}
	return sum;
}
